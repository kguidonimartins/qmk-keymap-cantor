#pragma once

// Obins stock firmware has something similar to this already enabled, but disabled by default in QMK
#define PERMISSIVE_HOLD

#define CAPS_WORD_IDLE_TIMEOUT 5000  // Turn off Caps Word after 5 seconds.
#define UNICODE_SELECTED_MODES UC_LNX
#define UNICODE_KEY_LNX  LCTL(LSFT(KC_E))

// Configure the global tapping term (default: 200ms)
#define TAPPING_TERM 175

// Prevent normal rollover on alphas from accidentally triggering mods.
#define IGNORE_MOD_TAP_INTERRUPT

// Enable rapid switch from tap to hold, disables double tap hold auto-repeat.
// Disabling this allow the auto-repeat a key after the second tap
// (just because my bad habitats in vim on QWERTY layout.) But, the
// drawback here is the missfire mods while typing rolls.
/* #define TAPPING_FORCE_HOLD */
// Fortunaly, this a config per key! see `get_tapping_force_hold` in the keymap.c.
#define TAPPING_FORCE_HOLD_PER_KEY
