// -*- compile-command: "make" -*-
// Copyright 2022 Diego Palacios (@diepala)
// SPDX-License-Identifier: GPL-2.0

#include QMK_KEYBOARD_H

/* @see: https://getreuer.info/posts/keyboards/custom-shift-keys/index.html */
#include "features/custom_shift_keys.h"
/* @see: https://getreuer.info/posts/keyboards/autocorrection/index.html */
#include "features/autocorrection.h"
/* @see: https://getreuer.info/posts/keyboards/achordion/index.html */
/* @see: https://www.reddit.com/r/olkb/comments/v5gzvl/qmk_bilateral_combinations_and_ctrlc_ctrlv_ctrlr/ */
#include "features/achordion.h"

#include "features/unicodemap.h"

enum cantor_layers {
    BASE,
    FN1,
    FN2,
    NAV,
};

enum custom_keycodes {
    R_ASSIGN = SAFE_RANGE,
};


/* BEG: */
/* @see: https://gist.githubusercontent.com/frederik-h/578524281a0b63ab143de1acd1d30bee/raw/f2a6a223275c6a1de59ae0fae2b69e2513c7e62f/extended_space_cadet_shift.c */
typedef struct {
  bool is_press_action;
  int state;
} tap;

enum {
  SINGLE_HOLD     = 1,
  SINGLE_TAP      = 2,
  SINGLE_TAP_HOLD = 3,
  DOUBLE_TAP      = 4,
  DOUBLE_TAP_HOLD = 5,
  TRIPLE_TAP      = 6,
  TRIPLE_TAP_HOLD = 7,
  QUAD_TAP        = 8,
};

enum {
  QUAD_TAP_DANCE_LEFT_SHIFT = 0,
  QUAD_TAP_DANCE_RIGHT_SHIFT = 1,
  SOME_OTHER_DANCE
};

int cur_dance (qk_tap_dance_state_t *state) {
  if(state->pressed)
    return SINGLE_HOLD;

  if (state->count == 1) {
      if(state->pressed) return SINGLE_TAP_HOLD;
      else return SINGLE_TAP;
  }

  if (state->count == 2) {
      if (state->pressed) return DOUBLE_TAP_HOLD;
      else return DOUBLE_TAP;
  }

  if (state->count == 3) {
      if (state->pressed) return TRIPLE_TAP_HOLD;
      else return TRIPLE_TAP;
  }

  if (state->count == 4)
    return QUAD_TAP;

   return -1;
}

static int quad_tap_dance_left_shift_tap_state = 0;
static int quad_tap_dance_right_shift_tap_state = 0;

void quad_tap_dance_left_shift_finished (qk_tap_dance_state_t *state, void *user_data) {
  quad_tap_dance_left_shift_tap_state = cur_dance(state);
  switch (quad_tap_dance_left_shift_tap_state) {
  case SINGLE_TAP:  // send (
    register_mods(MOD_BIT(KC_LSFT));
    register_code(KC_9);
    break;
  case DOUBLE_TAP:  // send [
    unregister_code(KC_9);
    register_code(KC_LBRC);
    break;
  case TRIPLE_TAP:  // send {
    unregister_code(KC_9);
    register_mods(MOD_BIT(KC_LSFT));
    register_code(KC_LBRC);
    break;
  case QUAD_TAP:    // send <
    unregister_code(KC_9);
    register_mods(MOD_BIT(KC_LSFT));
    register_code(KC_COMM);
    break;
  case SINGLE_HOLD: // normal left shift
    unregister_code(KC_9);
    register_mods(MOD_BIT(KC_LSFT));
  }
}

void quad_tap_dance_left_shift_reset (qk_tap_dance_state_t *state, void *user_data) {
  switch (quad_tap_dance_left_shift_tap_state) {
  case SINGLE_TAP:
    unregister_code(KC_9);
    unregister_mods(MOD_BIT(KC_LSFT));
    break;
  case DOUBLE_TAP:
    unregister_code(KC_LBRC);
    break;
  case TRIPLE_TAP:
    unregister_code(KC_LBRC);
    unregister_mods(MOD_BIT(KC_LSFT));
    break;
  case QUAD_TAP:
    unregister_code(KC_COMM);
    unregister_mods(MOD_BIT(KC_LSFT));
    break;
  case SINGLE_HOLD:
    unregister_mods(MOD_BIT(KC_LSFT));
    break;
  }
  quad_tap_dance_left_shift_tap_state = 0;
}


void quad_tap_dance_right_shift_finished (qk_tap_dance_state_t *state, void *user_data) {
  quad_tap_dance_right_shift_tap_state = cur_dance(state);
  switch (quad_tap_dance_right_shift_tap_state) {
  case SINGLE_TAP:  // send )
    register_mods(MOD_BIT(KC_RSFT));
    register_code(KC_0);
    break;
  case DOUBLE_TAP:  // send ]
    unregister_code(KC_0);
    register_code(KC_RBRC);
    break;
  case TRIPLE_TAP:  // send }
    unregister_code(KC_0);
    register_mods(MOD_BIT(KC_RSFT));
    register_code(KC_RBRC);
    break;
  case QUAD_TAP:    // send >
    unregister_code(KC_0);
    register_mods(MOD_BIT(KC_RSFT));
    register_code(KC_DOT);
    break;
  case SINGLE_HOLD: // normal right shift
    unregister_code(KC_0);
    register_mods(MOD_BIT(KC_RSFT));
  }
}

void quad_tap_dance_right_shift_reset (qk_tap_dance_state_t *state, void *user_data) {
  switch (quad_tap_dance_right_shift_tap_state) {
  case SINGLE_TAP:
    unregister_code(KC_0);
    unregister_mods(MOD_BIT(KC_RSFT));
    break;
  case DOUBLE_TAP:
    unregister_code(KC_RBRC);
    break;
  case TRIPLE_TAP:
    unregister_code(KC_RBRC);
    unregister_mods(MOD_BIT(KC_RSFT));
    break;
  case QUAD_TAP:
    unregister_code(KC_DOT);
    unregister_mods(MOD_BIT(KC_RSFT));
    break;
  case SINGLE_HOLD:
    unregister_mods(MOD_BIT(KC_RSFT));
    break;
  }
  quad_tap_dance_right_shift_tap_state = 0;
}

qk_tap_dance_action_t tap_dance_actions[] = {
  [QUAD_TAP_DANCE_LEFT_SHIFT]  = ACTION_TAP_DANCE_FN_ADVANCED(NULL, quad_tap_dance_left_shift_finished, quad_tap_dance_left_shift_reset),
  [QUAD_TAP_DANCE_RIGHT_SHIFT] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, quad_tap_dance_right_shift_finished, quad_tap_dance_right_shift_reset),

};

/* @see: https://gist.githubusercontent.com/frederik-h/578524281a0b63ab143de1acd1d30bee/raw/f2a6a223275c6a1de59ae0fae2b69e2513c7e62f/extended_space_cadet_shift.c */
/* END: */

/* tap_dance on shift keys */
#define LS_TD    TD(QUAD_TAP_DANCE_LEFT_SHIFT)
#define RS_TD    TD(QUAD_TAP_DANCE_RIGHT_SHIFT)

/* []{} on zxcv */
#define LSQBKT   KC_LBRC
#define RSQBKT   KC_RBRC
#define LSQCKT   S(KC_LBRC)
#define RSQCKT   S(KC_RBRC)

/* navigation and symbols on external thumb cluster when hold, but*/
/* enter when tap on the left */
#define NAV_SYML LT(NAV, KC_ENT)
/* space when tap on the right */
#define NAV_SYMR LT(NAV, KC_SPC)

#define TAB_GUI  LGUI_T(KC_TAB)
#define CTL_ESC  CTL_T(KC_ESC)
#define CTL_ENT  CTL_T(KC_ENT)
#define CTL_DOT  CTL_T(KC_DOT)
#define ALT_TAB  ALT_T(KC_TAB)

/*  FIXME 2022-07-13: external left key stop working */
#define CTL_ENT   CTL_T(KC_ENT)
#define NUMB_PAD LT(FN1, KC_BSPC)
#define LALT_BKP LT(KC_LALT, KC_BSPC)
#define FUN_KEYS MO(FN2)

/* Define R REPL code execution in Emacs */
#define SFT_ENT S(KC_ENT)     // send Shift+Enter
#define CRT_ENT C(KC_ENT)     // send Control+Enter
#define CRT_BSL C(KC_BSLS)    // send Control+Backslash
#define CST_BSL RCS(KC_BSLS)  // send Control+Shift+Backslash

// Left-hand home row mods
#define HOME_A LGUI_T(KC_A)
#define HOME_S LALT_T(KC_S)
#define HOME_D LSFT_T(KC_D)
#define HOME_F LCTL_T(KC_F)
#define HOME_Z LCTL_T(KC_Z)

// Right-hand home row mods
#define HOME_J RCTL_T(KC_J)
#define HOME_K RSFT_T(KC_K)
#define HOME_L LALT_T(KC_L)
#define SL_GUI RGUI_T(KC_SLSH) // I really like having the : instead ; in this position

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [BASE] = LAYOUT_split_3x6_3(
//,-------- --------- --------- --------- --------- --------- .                      ,-------- --------- --------- --------- --------- --------- .
    TAB_GUI ,    KC_Q ,    KC_W ,    KC_E ,    KC_R ,    KC_T ,                           KC_Y ,    KC_U ,    KC_I ,    KC_O ,   KC_P  , KC_BSPC ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    CTL_ESC ,  HOME_A ,  HOME_S ,  HOME_D ,  HOME_F ,    KC_G ,                           KC_H ,  HOME_J ,  HOME_K ,  HOME_L , KC_COLN , KC_DQT  ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    LS_TD   ,  HOME_Z ,    KC_X ,    KC_C ,    KC_V ,    KC_B ,                           KC_N ,    KC_M , KC_COMM , CTL_DOT , SL_GUI  ,  RS_TD  ,
//|-------- +-------- +-------- +-------- +-------- +-------- +----------||----------+-------- +-------- +-------- +-------- +-------- +-------- |
                                            ALT_TAB , CTL_ENT , NAV_SYML ,   NAV_SYMR, NUMB_PAD, FUN_KEYS
                                    //    '------------------------------||------------------------------'
),

[FN1] = LAYOUT_split_3x6_3(
//,-------- --------- --------- --------- --------- --------- .                      ,-------- --------- --------- --------- --------- --------- .
    _______ , KC_LBRC ,    KC_7 ,    KC_8 ,    KC_9 , KC_RBRC ,                        _______ , _______ , _______ , _______ , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ , S(KC_9) ,    KC_4 ,    KC_5 ,    KC_6 , S(KC_0) ,                        _______ , KC_LCTL , KC_LSFT , KC_LALT , KC_LGUI , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ ,S(KC_LBRC),   KC_1 ,    KC_2 ,   KC_3 ,S(KC_RBRC),                        KC_PASTE, KC_COPY , _______ , _______ , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- +---------|   |--------+-------- +-------- +-------- +-------- +-------- +-------- |
                                            _______ ,    KC_0 , _______ ,     _______, _______ , _______
                                    //    '-----------------------------|   |----------------------------'
),

[FN2] = LAYOUT_split_3x6_3(
//,-------- --------- --------- --------- --------- --------- .                      ,-------- --------- --------- --------- --------- --------- .
    _______ ,  KC_F12 ,   KC_F7 ,   KC_F8 ,   KC_F9 , _______ ,                        _______ , _______ , _______ , _______ , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ ,  KC_F11 ,   KC_F4 ,   KC_F5 ,   KC_F6 , _______ ,                        _______ , _______ , _______ , _______ , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ ,  KC_F10 ,   KC_F1 ,   KC_F2 ,   KC_F3 , _______ ,                        _______ , _______ , _______ , _______ , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- +---------|   |--------+-------- +-------- +-------- +-------- +-------- +-------- |
                                            _______ ,  KC_F10 , _______ ,     _______, _______ , _______
                                    //    '-----------------------------|   |----------------------------'
),

[NAV] = LAYOUT_split_3x6_3(
//,-------- --------- --------- --------- --------- --------- .                      ,-------- --------- --------- --------- --------- --------- .
     KC_GRV , KC_EXLM ,   KC_AT , KC_HASH ,  KC_DLR , KC_PERC ,                        KC_CIRC , KC_UNDS , KC_MINS ,  KC_EQL ,KC_PPLS  , KC_0    ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
    _______ , KC_AMPR , KC_ASTR , KC_BSLS , KC_PIPE , R_ASSIGN,                        KC_LEFT , KC_DOWN , KC_UP   , KC_RGHT ,  KC_DQT , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- |                      |-------- +-------- +-------- +-------- +-------- +-------- |
     _______ , LSQBKT  , RSQBKT  , LSQCKT  , RSQCKT  ,S(KC_9) ,                        CRT_ENT , SFT_ENT , CRT_BSL , CST_BSL , _______ , _______ ,
//|-------- +-------- +-------- +-------- +-------- +-------- +---------|   |--------+-------- +-------- +-------- +-------- +-------- +-------- |
                                            _______ , _______ , _______ ,    _______ , _______ , _______
                                    //    '-----------------------------|   |----------------------------'
)

};

bool get_tapping_force_hold(uint16_t keycode, keyrecord_t* record) {
    // @see: https://getreuer.info/posts/keyboards/achordion/index.html
    // Enable PERMISSIVE_HOLD!
    // If you quickly hold a tap-hold key after tapping it, the tap action is
    // repeated. Key repeating is useful e.g. for Vim navigation keys, but can
    // lead to missed triggers in fast typing. Here, returning true means we
    // instead want to "force hold" and disable key repeating.
    switch (keycode) {
    // Repeating is useful for Vim navigation keys.
    case HOME_J:
    case HOME_K:
    case HOME_L:
        return false;  // Enable key repeating.
    default:
        return true;  // Otherwise, force hold and disable key repeating.
    }
}

uint16_t achordion_timeout(uint16_t tap_hold_keycode) {
  return 500;
}

/* Define exceptions for bilateral combinations of achordion lib */
/* @see: https://getreuer.info/posts/keyboards/achordion/index.html */
bool achordion_chord(uint16_t tap_hold_keycode,
                     keyrecord_t* tap_hold_record,
                     uint16_t other_keycode,
                     keyrecord_t* other_record) {

  // Exceptionally consider the following chords as holds, even though they
  // are on the same hand.

  switch (tap_hold_keycode) {


  // left hand
  // R3 left pinky
  case TAB_GUI:
      if (
          other_keycode == KC_Q ||
          other_keycode == KC_W ||
          other_keycode == KC_E ||
          other_keycode == KC_R ||
          other_keycode == KC_T ||
          other_keycode == HOME_A ||
          other_keycode == HOME_S ||
          other_keycode == HOME_D ||
          other_keycode == HOME_F ||
          other_keycode == KC_G ||
          other_keycode == HOME_Z ||
          other_keycode == KC_X ||
          other_keycode == KC_C ||
          other_keycode == KC_V ||
          other_keycode == KC_B ||
          other_keycode == CTL_ENT ||
          other_keycode == ALT_TAB
          ) {return true; }
      break;

  // R2 left pinky
  case CTL_ESC:
      if (
          other_keycode == KC_Q ||
          other_keycode == KC_W ||
          other_keycode == KC_E ||
          other_keycode == KC_R ||
          other_keycode == KC_T ||
          other_keycode == HOME_A ||
          other_keycode == HOME_S ||
          other_keycode == HOME_D ||
          other_keycode == HOME_F ||
          other_keycode == KC_G ||
          other_keycode == HOME_Z ||
          other_keycode == KC_X ||
          other_keycode == KC_C ||
          other_keycode == KC_V ||
          other_keycode == KC_B ||
          other_keycode == CTL_ENT ||
          other_keycode == ALT_TAB
          ) {return true; }
      break;

  // left middle thumb
  case CTL_ENT:
      if (
          other_keycode == KC_Q ||
          other_keycode == KC_W ||
          other_keycode == KC_E ||
          other_keycode == KC_R ||
          other_keycode == KC_T ||
          other_keycode == HOME_A ||
          other_keycode == HOME_S ||
          other_keycode == HOME_D ||
          other_keycode == HOME_F ||
          other_keycode == KC_G ||
          other_keycode == HOME_Z ||
          other_keycode == KC_X ||
          other_keycode == KC_C ||
          other_keycode == KC_V ||
          other_keycode == KC_B ||
          other_keycode == CTL_ENT ||
          other_keycode == ALT_TAB
          ) { return true; }
      break;

  // R1 left pinky
  case HOME_Z:
      if (
          other_keycode == HOME_S ||
          other_keycode == HOME_D ||
          other_keycode == HOME_F ||
          other_keycode == KC_G ||
          other_keycode == KC_X ||
          other_keycode == KC_C ||
          other_keycode == KC_V ||
          other_keycode == KC_B
          ) { return true; }
      break;

  // right hand
  // right out thumb
  case NAV_SYMR:
      if (
          other_keycode == KC_Y ||
          other_keycode == KC_U ||
          other_keycode == KC_I ||
          other_keycode == KC_O ||
          other_keycode == KC_P ||
          other_keycode == KC_BSPC ||
          other_keycode == KC_H ||
          other_keycode == HOME_J ||
          other_keycode == HOME_K ||
          other_keycode == HOME_L ||
          other_keycode == KC_COLN
          ) { return true; }
      break;

  }

  // Also allow same-hand holds when the other key is in the rows below the
  // alphas. I need the `% (MATRIX_ROWS / 2)` because my keyboard is split.
  if (other_record->event.key.row % (MATRIX_ROWS / 2) >= 3) { return true; }

  // Otherwise, follow the opposite hands rule.
  return achordion_opposite_hands(tap_hold_record, other_record);

}

/* @see: https://getreuer.info/posts/keyboards/custom-shift-keys/index.html */
const custom_shift_key_t custom_shift_keys[] = {
  {KC_DQT,  KC_QUOT }, // Shift " is '
  {KC_COLN, KC_SCLN}, // Shift : is ;
};

uint8_t NUM_CUSTOM_SHIFT_KEYS =
    sizeof(custom_shift_keys) / sizeof(custom_shift_key_t);

enum combo_events {
  // . and C => activate Caps Word.
  CAPS_COMBO,
  COMBO_LENGTH
};
uint16_t COMBO_LEN = COMBO_LENGTH;

const uint16_t caps_combo[] PROGMEM = {KC_COMM, KC_C, COMBO_END};

combo_t key_combos[] = {
  [CAPS_COMBO] = COMBO_ACTION(caps_combo),
};

void process_combo_event(uint16_t combo_index, bool pressed) {
  if (pressed) {
    switch(combo_index) {

      case CAPS_COMBO:
        caps_word_on();  // Activate Caps Word.
        break;

    }
  }
}

bool process_record_user(uint16_t keycode, keyrecord_t* record) {
    if (!process_achordion(keycode, record)) { return false; }
    if (!process_custom_shift_keys(keycode, record)) { return false; }
    if (!process_autocorrection(keycode, record)) { return false; }
    switch (keycode) {
    case R_ASSIGN:
        if (record->event.pressed) {
            // when keycode R_ASSIGN is pressed
            SEND_STRING(" <-");
        } else {
            // when keycode R_ASSIGN is released
        }
        break;
    }
    return true;
}

void matrix_scan_user(void) {
  achordion_task();
}
